class CreateCars < ActiveRecord::Migration[7.0]
  def change
    create_table :cars do |t|
      t.string :name
      t.string :fueltype
      t.string :gearbox
      t.decimal :price

      t.timestamps
    end
  end
end
