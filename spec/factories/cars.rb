FactoryBot.define do
  factory :car do
    name { "MyString" }
    fueltype { "MyString" }
    gearbox { "MyString" }
    price { "9.99" }
  end
end
